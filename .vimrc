call plug#begin('~/.vim/plugged')
Plug 'othree/html5.vim'
Plug 'valloric/youcompleteme'
Plug 'townk/vim-autoclose'
Plug 'mxw/vim-jsx'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'SirVer/ultisnips'
Plug 'justinj/vim-react-snippets'
Plug 'scrooloose/nerdtree'
Plug 'vim-scripts/loremipsum'
Plug 'lilydjwg/colorizer'
Plug 'potatoesmaster/i3-vim-syntax'
call plug#end()


syntax on
colorscheme panic 
set number
set encoding=utf-8
set tabstop=2
set guifont=FuraCode:s/NF
nnoremap <silent> <ESC>OA <UP>
nnoremap <silent> <ESC>OB <DOWN>
nnoremap <silent> <ESC>OC <RIGHT>
nnoremap <silent> <ESC>OD <LEFT>
inoremap <silent> <ESC>OA <UP>
inoremap <silent> <ESC>OB <DOWN>
inoremap <silent> <ESC>OC <RIGHT>
inoremap <silent> <ESC>OD <LEFT>

"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"UltiSnips triggering
let g:UltiSnipsExpandTrigger = '<C-q>'
let g:UltiSnipsJumpForwardTrigger = '<C-j>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'

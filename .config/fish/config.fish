#!/bin/bash

set -U fish_greeting
# Start X at login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR = 1
        exec startx -- -keeptty > /dev/null
    end 
end


alias ls='lsd'

alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'

alias fm='python ~/.ranger/ranger.py'

alias uf='fc-cache -f -v'

alias weather='curl wttr.in'

alias download='wget -r -nd $1'

alias start='sudo systemctl start $1'
alias stop='sudo systemctl stop $1'
alias disable='sudo systemctl disable $1'
alias enable='sudo systemctl enable $1'
alias service-status='sudo systemctl status $1'
alias restart='sudo systemctl restart $1'


export COUNTRY=CN
# export PATH="$PATH:$HOME/npm/bin"
# export NODE_PATH="$NODE_PATH:$HOME/npm/lib/node_modules"
# export PATH="/home/simson/.localbin:$PATH"
